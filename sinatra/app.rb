require 'sinatra'
require 'json'
require_relative 'model/chopper'

get '/sum' do
  chopper = Chopper.new
  x = params["x"]
  param = x.split(',').map { |num| num.to_i }
  result = chopper.sum(param)
  content_type :json
  { :sum => "#{x}", :resultado => "#{result}" }.to_json
end

post '/chop' do
  chopper = Chopper.new
  x = params["x"]
  y = params["y"]
  result = chopper.chop(x.to_i, y.split(',').map { |num| num.to_i })
  content_type :json
  { :chop => 'x='+x+', y='+y , :resultado => result}.to_json
end

