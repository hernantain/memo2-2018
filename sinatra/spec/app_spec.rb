# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "get /sum?x=9,9 should return uno,ocho" do
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include "uno,ocho"
  end

  it "get /sum?x=1 should return uno" do
    get '/sum?x=1'
    expect(last_response).to be_ok
    expect(last_response.body).to include "uno"
  end

  it "get /sum?x=50,50 should return demasiado grande" do
    get '/sum?x=50,50'
    expect(last_response).to be_ok
    expect(last_response.body).to include "demasiado grande"
  end

  it "get /sum?x=20,1,1,1 should return dos,tres" do
    get '/sum?x=20,1,1,1'
    expect(last_response).to be_ok
    expect(last_response.body).to include "dos,tres"
  end

  it "get /chop?x=3&y=0,7,3 should return dos,tres" do
    post '/chop', {'x':'3','y':'0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '2'
  end

  it "get /chop?x=4&y=0,1,2,7,4 should return dos,tres" do
    post '/chop', {'x':'4','y':'0,1,2,7,4'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '4'
  end

  it "get /chop?x=4&y=0,1,2,7,4 should return dos,tres" do
    post '/chop', {'x':'3','y':'0,1,2,7,4'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '-1'
  end  

end

# curl localhost:4567/hola?nombre=nico
# curl -X POST localhost:4567/chau -d "nombre=nico"