class Chopper

  def chop(n, array)
    if array.include? n
      return array.index(n)
    end
    return -1
  end


  def get_string_number( num )

    dic = { 0 => 'cero',
            1 => 'uno',
        2 => 'dos',
        3 => 'tres',
        4 => 'cuatro',
        5 => 'cinco',
        6 => 'seis',
        7 => 'siete',
        8 => 'ocho',
        9 => 'nueve'}

    return dic[num]
  end


  def sum( array )

    if !array.any?
      return 'vacio'
    end

    number = 0  
    array.each{ |x| number+= x }
    if number < 10
      return get_string_number(number)
    end

    if number > 99
      return 'demasiado grande'
    end

    first_digit = get_string_number(number/10)
    second_digit = get_string_number(number%10)

    string = first_digit
    string << ','
    string << second_digit

    return string
  end
end
