require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "get /total?c=10&p=50&e=UT should return" do
    get '/total?c=10&p=50&e=UT'
    expect(last_response).to be_ok
    expect(last_response.body).to include "534.25"
  end

  it "get /total?c=100&p=50&e=NV should return" do
    get '/total?c=100&p=50&e=NV'
    expect(last_response).to be_ok
    expect(last_response.body).to include "5130"
  end

end