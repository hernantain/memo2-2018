require 'rspec' 
require_relative '../model/elephant'

describe 'elephant' do

  let(:elephant) { Elephant.new }  
  
  it 'Calcular total sin descuento con impuesto para UT' do
  	expect(elephant.calculate(1,100,'UT')).to eq 106.85
  end

  it 'Calcular total sin descuento con impuesto para NV' do 
  	expect(elephant.calculate(1,100,'NV')).to eq 108
  end

  it 'Calcular total sin descuento con impuesto para TX' do 
  	expect(elephant.calculate(1,100,'TX')).to eq 106.25
  end

  it 'Calcular total sin descuento con impuesto para AL' do 
  	expect(elephant.calculate(1,100,'AL')).to eq 104
  end

  it 'Calcular total sin descuento con impuesto para CA' do 
  	expect(elephant.calculate(1,100,'CA')).to eq 108.25
  end

  it 'Calcular total con el primer descuento con impuesto para UT' do 
  	expect(elephant.calculate(10,100,'UT')).to eq 1036.445
  end

  it 'Calcular total con el segundo descuento con impuesto para NV' do 
    expect(elephant.calculate(10,500,'NV')).to eq 5130
  end

  it 'Calcular total con el tercer descuento con impuesto para TX' do 
    expect(elephant.calculate(10,700,'TX')).to eq 6916.875
  end

  it 'Calcular total con el cuarto descuento con impuesto para AL' do 
    expect(elephant.calculate(10,1000,'AL')).to eq 9360
  end

  it 'Calcular total con el quinto descuento con impuesto para CA' do 
    expect(elephant.calculate(10,5000,'CA')).to eq 46006.25
  end
end
