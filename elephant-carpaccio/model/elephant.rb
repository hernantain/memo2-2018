class Elephant

  @@discount_dic = {
    0...1000 => 1,
    1000...5000 => 0.97,
    5000...7000 => 0.95, 
    7000...10000 => 0.93,
    10000...50000 => 0.9,
    50000... Float::INFINITY => 0.85
  }

  @@states_dic = {
    'UT' => 1.0685,
    'NV' => 1.08,
    'TX' => 1.0625,
    'AL' => 1.04,
    'CA' => 1.0825
  }

  def calculate(num_tickets, price, state)
    total = num_tickets * price
    total *= @@discount_dic.select { |key| key === total }.values.first
    (total * @@states_dic[state]).round(3)
  end

end
