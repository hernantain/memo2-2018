require 'sinatra'
require 'json'
require_relative 'model/elephant'

get '/total' do
  elephant = Elephant.new
  result = elephant.calculate(params["c"].to_i,params["p"].to_f,params["e"])
  content_type :json
  { :resultado => "#{result}" }.to_json
end
