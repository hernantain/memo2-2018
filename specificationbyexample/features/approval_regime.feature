Feature: Approval regime

  Background: 
  Given the student Juan
  And the individual homework "ta"
  And the individual homework "tb"
  And the individual homework "tc"
  And the group project

  Scenario: approved student
  Given the regular student Juan
  When he approved all his weekly homeworks
  And he assisted to the 75 % of the classes
  And he approved at least 3 iterations of the group project
  Then he approved the subject

  Scenario: Dissapproved student for absences
  Given the regular student Juan
  When he approved all his weekly homeworks
  And he assisted to the 60 % of the classes
  And he approved at least 3 iterations of the group project
  Then he dissapproved the subject
  
  Scenario: Dissapproved student for individual homeworks
  Given the regular student Juan
  When he dissapproved the individual homework tc
  Then he dissapproved the subject

  Scenario: Dissapproved student for group project
  Given the regular student Juan
  When he approved all his weekly homeworks
  And he assisted to the 75 % of the classes
  And approved 2 iterations of the group project
  Then he dissapproved the subject
