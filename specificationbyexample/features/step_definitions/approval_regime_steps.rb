Given(/^the student Juan$/) do
  @student = Student.new
end

Given(/^the individual homework "([^"]*)"$/) do |homework_name|
  @student.add_homework( Homework.new(homework_name) )
end

Given(/^the group project$/) do
  @project = GroupProject.new
end

Given(/^the regular student Juan$/) do
  @student.is_regular = true 
end


When(/^he approved all his weekly homeworks$/) do
  @student.homeworks.each{|hwk| hwk.grade = 7 }
end

When(/^he assisted to the (\d+) % of the classes$/) do |assistance|
  @student.assistance = assistance.to_i
end

When(/^he approved at least (\d+) iterations of the group project$/) do |no_iterations_approved|
  @project.approved_iterations = no_iterations_approved.to_i
  @student.project = @project
end

Then(/^he approved the subject$/) do
  approved = @student.subject_approved?
  expect(approved).to eq true
end

Then(/^he dissapproved the subject$/) do
  approved = @student.subject_approved?
  expect(approved).to eq false
end

When(/^he dissapproved the individual homework tc$/) do
  @student.homeworks.each{|hwk| hwk.name.eql? "tc" ? hwk.grade = 2 : hwk.grade = 5 }
end

When(/^approved (\d+) iterations of the group project$/) do |no_iterations_approved|
  @project.approved_iterations = no_iterations_approved.to_i
  @student.project = @project
end
