require 'rspec' 
require_relative '../model/student'
require_relative '../model/homework'
require_relative '../model/group_project'


describe 'specificationbyexample' do
  
  let(:student) { Student.new } 
  let(:ta) { Homework.new("ta") } 
  let(:tb) { Homework.new("tb") } 
  let(:tc) { Homework.new("tc") } 
  let(:project) {GroupProject.new}


  it 'tarea aprobada' do
  	ta.grade = 5
    expect(ta.approved?).to eq true
  end

  it 'tarea desaprobada' do
    ta.grade = 2
    expect(ta.approved?).to eq false
  end

  it 'Proyecto grupal aprobado' do  
    project.approved_iterations = 3
    expect(project.approved?).to eq true
  end

  it 'Proyecto grupal desaprobado' do 
    project.approved_iterations = 2
    expect(project.approved?).to eq false
  end

  it 'Creo alumno y agrego tareas' do 
    student.add_homework(ta)
    student.add_homework(tb)
    student.add_homework(tc)
    expect(student.homeworks).to eq [ta, tb, tc]
  end

  it 'Creo alumno y agrego proyecto' do 
    student.project = project
    expect(student.project).to eq project
  end

  it 'Alumno cumple todos los requisitos de aprobacio - Scenario 1' do
    student.add_homework(ta)
    student.add_homework(tb)
    student.add_homework(tc)  
    student.homeworks.each{ |hwk| hwk.grade = 4}
    project.approved_iterations = 3
    student.assistance = 75
    student.project = project
    approved = student.subject_approved?
    expect(approved).to eq true
  end

  it 'Alumno desaprueba por asistencia - Scenario 2' do 
    student.add_homework(ta)
    student.add_homework(tb)
    student.add_homework(tc)
    student.homeworks.each{ |hwk| hwk.grade = 4}
    project.approved_iterations = 3
    student.assistance = 60
    student.project = project
    approved = student.subject_approved?
    expect(approved).to eq false
  end

  it 'Alumno desaprueba por no tener aprobada tc - Scenario 3' do 
    student.add_homework(ta)
    student.add_homework(tb)
    student.add_homework(tc)
    student.homeworks.each{|hwk| hwk.name.eql? "tc" ? hwk.grade = 2 : hwk.grade = 4 }
    approved = student.subject_approved?
    expect(approved).to eq false
  end

  it 'Alumno desaprueba por no tener la cantidad minima de iteraciones aprobadas - Scenario 4' do 
    student.add_homework(ta)
    student.add_homework(tb)
    student.add_homework(tc)
    student.homeworks.each{ |hwk| hwk.grade = 8}
    project.approved_iterations = 2
    student.assistance = 75
    student.project = project
    approved = student.subject_approved?
    expect(approved).to eq false
  end

end