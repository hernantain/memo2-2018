MIN_GRADE = 4

class Homework
  attr_accessor :grade,:name
  @name

  def initialize(name, grade = 4)
    @name = name
  end

  def approved?
    grade >= MIN_GRADE
  end
end