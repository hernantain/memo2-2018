MIN_ASSISTANCE = 75

class Student

  attr_accessor :homeworks,:assistance,:project,:is_regular
  @homeworks

  def initialize
    @homeworks = Array.new
  end

  def add_homework(hwk)
    @homeworks.push(hwk)
  end

  def subject_approved?
    homeworks_approved? and assistance >= MIN_ASSISTANCE and project.approved?
  end

  private

    def homeworks_approved?
      @homeworks.all? &:approved?
    end  
end
