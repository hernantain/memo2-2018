MIN_APPROVED_ITERATIONS = 3

class GroupProject
  attr_accessor :approved_iterations

  def approved?
    approved_iterations >= MIN_APPROVED_ITERATIONS
  end
end
